#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

#define PLUGIN_VERSION 	"1.0"

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define MAX_EDICTS		2048

int g_iSoundEnts[MAX_EDICTS];
int g_iNumSounds;

public Plugin myinfo = 
{
	name = "Roundend Stop Music",
	author = ".#Zipcore, Credits: thecount",
	description = "You know what this is.",
	version = PLUGIN_VERSION,
	url = "zipcore.net"
};

public void OnPluginStart()
{
	CreateConVar("sm_roundend_stopmusic_version", PLUGIN_VERSION, "Roundend Stop Map Music", FCVAR_PLUGIN|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	
	HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
	HookEvent("round_end", Event_RoundEnd, EventHookMode_PostNoCopy);
}

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	UpdateSounds();
	
	return Plugin_Continue;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	StopMusic();
	
	return Plugin_Continue;
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if(!StrEqual(classname, "ambient_generic", false))
		return;
	
	char sSound[PLATFORM_MAX_PATH];
	GetEntPropString(entity, Prop_Data, "m_iszSound", sSound, sizeof(sSound));	
	
	int len = strlen(sSound);
	if (len > 4 && (StrEqual(sSound[len-3], "mp3") || StrEqual(sSound[len-3], "wav")))
		g_iSoundEnts[g_iNumSounds++] = EntIndexToEntRef(entity);
	else return;
}

void UpdateSounds()
{
	g_iNumSounds = 0;
	
	char sSound[PLATFORM_MAX_PATH];
	int entity = INVALID_ENT_REFERENCE;
	while ((entity = FindEntityByClassname(entity, "ambient_generic")) != INVALID_ENT_REFERENCE)
	{
		GetEntPropString(entity, Prop_Data, "m_iszSound", sSound, sizeof(sSound));
		
		int len = strlen(sSound);
		if (len > 4 && (StrEqual(sSound[len-3], "mp3") || StrEqual(sSound[len-3], "wav")))
			g_iSoundEnts[g_iNumSounds++] = EntIndexToEntRef(entity);
	}
}

void StopMusic()
{
	char sSound[PLATFORM_MAX_PATH], entity;
	
	for (int i = 0; i < g_iNumSounds; i++)
	{
		entity = EntRefToEntIndex(g_iSoundEnts[i]);
		
		if (entity != INVALID_ENT_REFERENCE)
		{
			GetEntPropString(entity, Prop_Data, "m_iszSound", sSound, sizeof(sSound));
			
			LoopIngameClients(client)
				Client_StopSound(client, entity, SNDCHAN_STATIC, sSound);
		}
	}
}

stock void Client_StopSound(int client, int entity, int channel, const char[] name)
{
	EmitSoundToClient(client, name, entity, channel, SNDLEVEL_NONE, SND_STOP, 0.0, SNDPITCH_NORMAL, _, _, _, true);
}
